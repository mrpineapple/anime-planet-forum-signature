<?php 
/* Setting Standard Image to be: 500x150 
Of course, this is only a guideline and you don't need to follow it
*/


//dealing with PNG images
header("Content-type: image/png");

//get_string_between("I am so Great","I","Great")
//result = " am so "
function get_string_between($string,$start,$end){
         $string = strstr($string,$start);
         $end = strpos($string,$end);
         $result = substr($string,strlen($start),$end-strlen($start) );
         return $result;
}

//Count How many Images we have
$images = scandir("Signature");
unset($images[0]);  //remove "." & ".."
unset($images[1]);
$image_count = count($images);

$choice = rand(1,$image_count);          

$filename = ("Signature/".$images[$choice + 1]);           
$image = imagecreatefrompng($filename); 
$size = getimagesize($filename);

//REPLACE THIS WITH YOUR USERNAME
$string = "mrpineapple";


$color = imagecolorallocate($image, 255, 100, 167);

imagefilledrectangle($image, (($size[0] - strlen($string)*6)/2), 0, (($size[0]/2 + strlen($string)*5)), 15, $color); //($image , $x1 , $y1 ,$x2 , $y2 , $color )

//In format : ($image, fontsize, rightindent, downindent, data, txtcolour)
imagestring($image, 3, (($size[0] - strlen($string)*5)/2), 5, $string, $font_black); 

//------Time Spent-----//
$url = "http://www.anime-planet.com/users/{$string}";
$raw = file_get_contents($url);


$parsed[0] = get_string_between($raw,"<li style=\"background-position: 0px 3px;\">","<span>Minute");
$parsed[0] = trim($parsed[0]);  //this program does NOT like printing nonprint chars

$parsed[1] = get_string_between($raw,"<li style=\"background-position: 0px 3px;\">","<span>Hour");
$parsed[1] = rtrim($parsed[1]);
$parsed[1] = substr($parsed[1],-2);
$parsed[1] = ltrim($parsed[1]);

$parsed[2] = get_string_between($raw,"<li style=\"background-position: 0px 3px;\">","<span>Day");
$parsed[2] = rtrim($parsed[2]);
$parsed[2] = substr($parsed[2],-2);
$parsed[2] = ltrim($parsed[2]);

$parsed[3] = get_string_between($raw,"<li style=\"background-position: -15px 3px;\">","<span>Week");
$parsed[3] = trim($parsed[3]);

$parsed[4] = get_string_between($raw,"<li style=\"background-position: -60px 3px;\">","<span>Month");
$parsed[4] = trim($parsed[4]);

//this one's not working for some reason :(
$parsed[5] = get_string_between($raw,"<li style=\"background-position: -60px 3px;\">","<span>Year");
$parsed[5] = rtrim($parsed[5]);
$parsed[5] = substr($parsed[5],-2);
$parsed[5] = ltrim($parsed[5]);

$parsed[0] = $parsed[0] . " Minutes";
$parsed[1] = $parsed[1] . " Hours";
$parsed[2] = $parsed[2] . " Days";
$parsed[3] = $parsed[3] . " Weeks";
$parsed[4] = $parsed[4] . " Months";
$parsed[5] = $parsed[5] . " Years";

//box
$color = imagecolorallocate($image, 255, 105, 180);
imagefilledrectangle($image, 0, $size[1] - 17, $size[0], $size[1], $color);
                         
imagestring($image, 3, 0, ($size[1] - 15), $parsed[0], $font_black); 
imagestring($image, 3, (($size[0] - strlen($parsed[0])*5)/5.5 * 1), ($size[1] - 15), $parsed[1], $font_black); 
imagestring($image, 3, (($size[0] - strlen($parsed[0])*5)/5.5 * 2), ($size[1] - 15), $parsed[2], $font_black); 
imagestring($image, 3, (($size[0] - strlen($parsed[0])*5)/5.5 * 3), ($size[1] - 15), $parsed[3], $font_black); 
imagestring($image, 3, (($size[0] - strlen($parsed[0])*5)/5.5 * 4), ($size[1] - 15), $parsed[4], $font_black); 
imagestring($image, 3, (($size[0] - strlen($parsed[0])*5)/5.5 * 5), ($size[1] - 15), $parsed[5], $font_black); 
//----------------------------//      

//--------anime stats---------//



//box
$color = imagecolorallocate($image, 190, 0, 80);
imagefilledrectangle($image, 0, $size[1] - 95, 120, $size[1] - 80, $color);
$color = imagecolorallocate($image, 160, 0, 80);
imagefilledrectangle($image, 0, $size[1] - 80, 120, $size[1] - 18, $color);

$string = "Anime";
imagestring($image,3,3,$size[1]-92,$string,$font_black);

$astat = get_string_between($raw, "anime/watched'>", " watched</a></li>");
$astat .= " Watched";
imagestring($image,3,5,$size[1]-78,$astat,$font_black);

$astat = get_string_between($raw, "anime/watching'>", " watching</a></li>");
$astat .= " Watching";
imagestring($image,3,5,$size[1]-68,$astat,$font_black);

$astat = get_string_between($raw, "anime/wanttowatch'>", " want to watch</a></li>");
$astat .= " Want To Watch";
imagestring($image,3,5,$size[1]-58,$astat,$font_black);

$astat = get_string_between($raw, "anime/stalled'>", " stalled</a></li>");
$astat .= " Stalled";
imagestring($image,3,5,$size[1]-48,$astat,$font_black);

$astat = get_string_between($raw, "anime/dropped'>", " dropped</a></li>");
$astat .= " Dropped";
imagestring($image,3,5,$size[1]-38,$astat,$font_black);

$astat = get_string_between($raw, "anime/wontwatch'>", " won't watch</a></li>");
$astat .= " Won't Watch";
imagestring($image,3,5,$size[1]-28,$astat,$font_black);


//----------------------------//

//--------manga stats---------//

//box
$color = imagecolorallocate($image, 160, 0, 80);
imagefilledrectangle($image, $size[0] - 120, $size[1] - 80, $size[0], $size[1] - 18, $color);

$color = imagecolorallocate($image, 190, 0, 80);
imagefilledrectangle($image, $size[0] - 120, $size[1] - 95, $size[0], $size[1] - 80, $color);


$string = "Manga";
imagestring($image,3,$size[0] - 117 ,$size[1]-92,$string,$font_black);

$astat = get_string_between($raw, "manga/read'>", " read</a></li>");
$astat .= " Read";
imagestring($image,3,$size[0] - 115,$size[1]-78,$astat,$font_black);

$astat = get_string_between($raw, "manga/reading'>", " reading</a></li>");
$astat .= " Reading";
imagestring($image,3,$size[0] - 115,$size[1]-68,$astat,$font_black);

$astat = get_string_between($raw, "manga/wanttoread'>", " want to read</a></li>");
$astat .= " Want To Read";
imagestring($image,3,$size[0] - 115,$size[1]-58,$astat,$font_black);

$astat = get_string_between($raw, "manga/stalled'>", " stalled</a></li>");
$astat .= " Stalled";
imagestring($image,3,$size[0] - 115,$size[1]-48,$astat,$font_black);

$astat = get_string_between($raw, "manga/dropped'>", " dropped</a></li>");
$astat .= " Dropped";
imagestring($image,3,$size[0] - 115,$size[1]-38,$astat,$font_black);

$astat = get_string_between($raw, "manga/wontread'>", " won't read</a></li>");
$astat .= " Won't Read";
imagestring($image,3,$size[0] - 115,$size[1]-28,$astat,$font_black);


//----------------------------//

imagepng($image);
//imagepng($image,"sig.png"); //creates a hardcopy of the image if a forum doesn't let you include the .php url
imagedestroy($image); 

?>
